---
layout: markdown_page
title: "Underperformance"
---

At GitLab, we strive to hire smart people who can get things done. Though the bar
for getting hired is high, our interview process is imperfect and later on we may find that someone is not living up to expectations. We want people to be successful and give every opportunity for each individual to work effectively but at the same time, we need to balance the company’s needs and move quickly to manage areas of underperformance.

When it becomes clear to a manager that an individual isn't accomplishing
enough or working well with others, here are guidelines for how to handle
underperformance. Note that someone may be terminated without undertaking these steps if the damage caused by their actions is irreparable or goes against our core values and expected code of conduct. Please work with People Operations on all cases of underperformance to protect the organization and equally consider the side of the individual involved.

1) Manager communicates to direct report during one-on-one that team member
needs to improve. If there are extenuating circumstances some leeway may be
granted depending on the situation.  This is an area where People Operations can provide a sounding board / voice of reason.

2) If the lack of performance persists, the manager moves to documenting clear expectations for improvement in a performance improvement plan (PIP). The intention of a PIP is to support the individual in any way required to make their time at GitLab a positive experience but also to make clear that immediate and sustained improvement
is required. The Society for Human Resources Management (SHRM) has a [helpful guide](https://www.shrm.org/templatestools/howtoguides/pages/performanceimprovementplan.aspx) to review when
you this step is needed to push past the current performance issues.

A performance improvement plan includes the following:

   * Evaluation of current work
   * Clear direction including metrics and concrete goals to improve (e.g. finish X before Y)
   * Resources/coaching necessary to achieve goals

This [basic PIP template](https://docs.google.com/document/d/1AsVwUikcUofl58eLWhiEEUFJqtwgUQNdDo5lM98bP7o/edit) will provide a good start to creating the document. The template should be customized to fit the particular situation. All PIPs should be forwarded to the People Ops Generalist or Director for final review and approval before delivery. This step will help ensure consistency in the PIP process for any affected team member and to protect GitLab should legal claims arise as a result of termination.  


3) Team member gets time (2-4 weeks depending on the role and circumstances) to demonstrate improvement and meet specific goals outlined in the PIP. If sufficient improvement is not made but progress is headed in the right direction, a plan period may be extended at the discretion of the manager.  By design, a PIP is expected to support a successful and sustained improvement in performance.

4) Otherwise, the team member is let go or his/her contract is cancelled. It is not necessary to create a second PIP for the same performance issues within a reasonable period of time and after informing the team member that the unacceptable performance has resurfaced in writing (an email is fine). To begin the termination process, the manager should forward a recommendation for termination to their Executive team member and People Operations including the history of the PIP and the recurring performance issues.

5) Should the PIP result in a termination or if a termination (for the same performance issues) happens after a PIP has ended, the People Ops Generalist will work with the manager to document the following debrief.

   1. How could this outcome have been avoided?
   2. Were there early signs that were missed?
   3. In retrospect, what questions should have been asked to bring awareness and
      ownership to performance issues? For example, "How would you compare yourself relative to your peers?" People are surprisingly honest here.

The PIP process should be a discreet and positive interaction between a manager and their direct report. Information shared in a PIP should be kept confidential by both participants. If underperformance becomes a reason for termination, the individual should not be taken by surprise but the rest of the company should be.

If a person does need to be let go, work with People Operations to follow the process for [involuntary termination](/handbook/people-operations/#involuntary-terminations) and the [offboarding steps](/handbook/offboarding/).
